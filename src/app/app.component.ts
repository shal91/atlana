import { AfterViewChecked, Component, OnInit } from '@angular/core';

/*
  Interfaces
 */

import { Letter } from './interfaces/letter';

/*
  Services
 */
import { ModalService } from './services/modal.service';
import { ToggleMenuService } from './services/toggle-menu.service';
import { NavigateService } from './services/navigate.service';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from './services/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, AfterViewChecked {
  fragment: string;
  scrollInit = false;

  public letter: Letter = {
    name: '',
    email: '',
    company: '',
    message: ''
  };

  public modalState;
  public modalSwitch;


  ngOnInit () {
    this._activetedRoute.fragment.subscribe(fragment => {
      this.fragment = fragment;
    });
  }

  ngAfterViewChecked () {
    if (!this.scrollInit) {
      this._navigate.changeScrollValue();
      this.scrollInit = true;
    }
  }

  constructor (private _modal: ModalService,
               private _navigate: NavigateService,
               private _activetedRoute: ActivatedRoute,
               private _httpService: HttpService,
               private _toggleMenu: ToggleMenuService) {

    this.modalState = this._modal.state;
    this.modalSwitch = function (state) {
      this._modal.switchState(state);
    };

    this._navigate.scrollTo.subscribe(val => {
      const el = document.scrollingElement || document.documentElement;
      el.scrollTop = val;
    });

  }


  menuToggle (event, state) {
    if (state === 'left') {
      this._toggleMenu.toggle(true);
    }

    if (state === 'right') {
      this._toggleMenu.toggle(false);
    }
  }
}
