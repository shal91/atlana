import {Component, OnInit, ViewChild} from '@angular/core';
import { SendMessageModel } from '../../interfaces/send-message.model';
import { HttpService } from '../../services/http.service';
import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  @ViewChild('contactForm') contactForm;
  entity = new SendMessageModel();

  constructor(private _http: HttpService, private _modal: ModalService) {
  }

  ngOnInit() {
  }

  sendMessage() {
    this._http.sendMessage(this.entity).subscribe(res => {
      this._modal.switchState('success');
      this.entity = new SendMessageModel();
      this.contactForm.reset();
    }, err => {
      this._modal.switchState('error');
    });
  }

}
