import {Component, OnInit} from '@angular/core';
import {ApiService} from "../../services/api.service";

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.scss']
})
export class CareersComponent implements OnInit {
  careers:any = [];

  constructor(private _apiService: ApiService) {
    this._apiService.getCareers().subscribe((res) => {
      this.careers = res;
    })
  }

  ngOnInit() {
  }

}
