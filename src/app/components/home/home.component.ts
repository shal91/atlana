import { Component, OnInit } from '@angular/core';

// import { Project } from '../../interfaces/project';
// import { ServiceSingle } from '../../interfaces/service-single';
import { ApiService } from '../../services/api.service';
// import { ScreenScaleDirective} from '../../directives/screen-scale.directive';
import {forkJoin} from 'rxjs/observable/forkJoin';
import {NavigateService} from '../../services/navigate.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public projects: any = [];
  public workflow: any = [];
  public keypoints: any = [];
  public ourServices: any = [];
  public engagement: any = [];
  public objectKeys = Object.keys;

  constructor(private _apiService: ApiService, private _navigateService: NavigateService) {

    this._apiService.getProjects()
      .subscribe(data => this.projects = data);

    this._apiService.getServices()
      .subscribe(data => this.ourServices = data);

    this._apiService.getWorkflow()
      .subscribe(data => this.workflow = data);

    this._apiService.getKeyPoints()
      .subscribe(data => this.keypoints = data);

    this._apiService.getEngagementModels()
      .subscribe(data => this.engagement = data);

    forkJoin(this._apiService.getProjects(), this._apiService.getServices(),
      this._apiService.getWorkflow(), this._apiService.getKeyPoints() ).subscribe((results) => {
        this._navigateService.changeScrollValue();
    });
  }

  navClick(fragment) {
    this._navigateService.scrollToElem(fragment);
  }

  ngOnInit() {
  }

}
