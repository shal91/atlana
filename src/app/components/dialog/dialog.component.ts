import { Component, OnInit } from '@angular/core';

import { ModalService } from '../../services/modal.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
  public modalSwitch;
  public modalState;

  constructor (private _modal: ModalService) {
    this.modalState = this._modal.state;
    this.modalSwitch = function (state) {
      this._modal.switchState(state);
    };
    this._modal.state.subscribe((value) => this.modalState = value);
  }

  ngOnInit () {
  }

  close () {
    this.modalSwitch(null);
  }

}
