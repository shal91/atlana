import { Component, HostListener, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {
  private _items = [];
  public animateState = false;
  public animatePrev = false;
  public animateNext = false;

  /*
    Swipe actions
   */

  public touchedStart = 0;
  public touchActive = false;
  public transformValue = 0;

  @Input()
  set items (items) {
    this._items = items;
    this._carouselInit();
  }

  get items () {
    return this._items;
  }

  @HostListener('window:resize', [])
  onWindowResize () {
    const prev = this.state.size;
    this.initSize();
    if (prev !== this.state.size) {
      this._carouselInit();
    }
  }

  state = {
    size: 'lg',
    current: 0,
    step: 1,
  };

  private _settings = {
    size: {
      xs: 1,
      sm: 2,
      md: 3,
      lg: 3,
      all: 1
    },
    step: 'size'
  };

  initSize () {
    const width = window.innerWidth;
    if (width > 1024) {
      this.state.size = 'lg';
    } else if (width > 768) {
      this.state.size = 'md';
    } else if (width > 640) {
      this.state.size = 'sm';
    } else {
      this.state.size = 'xs';
    }
  }

  public itemsOrdered = [[]] = [[], [], []];

  private _carouselInit = () => {
    const length = this.items && this.items.length;
    if (length) {
      this.itemsOrdered = [[], [], []];
      const current = this.state.current;
      this.state.step = this._settings.step === 'size' ? this._settings.size[this.state.size] : this._settings.step;
      let size = this._settings.size[this.state.size] || this._settings.size['all'];
      for (let i = 0; i < size; i++) {
        this.itemsOrdered[0].push(this.items[(current - size + i) < 0 ? length + current - size + i : (current - size + i)]);
        this.itemsOrdered[1].push(this.items[current + i < length ? current + i : current + i - length]);
        this.itemsOrdered[2].push(this.items[current + size + i < length ? current + size + i : current + size + i - length]);
      }
    }
  };

  constructor () {
  }

  ngOnInit () {
    this.initSize();
    this._carouselInit();
  }

  prev () {
    this.animateState = true;
    this.animatePrev = true;
    const length = (this.items && this.items.length) || 0;
    const current = this.state.current;
    const step = this.state.step;

    if (current - step < 0) {
      this.state.current = length + current - step;
    } else {
      this.state.current = current - step;
    }

    setTimeout(() => {
      this.animateState = false;
      this.animatePrev = false;
      this._carouselInit();
    }, 600);
  }

  next () {
    this.animateState = true;
    this.animateNext = true;
    const length = (this.items && this.items.length) || 0;
    const current = this.state.current;
    const step = this.state.step;

    if (current + step + 1 > length) {
      this.state.current = current - length + step;
    } else {
      this.state.current = current + step;
    }

    setTimeout(() => {
      this.animateState = false;
      this.animateNext = false;
      this._carouselInit();
    }, 600);
  }


  @HostListener('touchstart', ['$event'])
  onTouchStart (event) {
    this.touchedStart = event.targetTouches[0].clientX;
    this.touchActive = true;
  }

  @HostListener('touchmove', ['$event'])
  onTouchMove (event) {
    const touchPosX = event.targetTouches[0].clientX;
    this.transformValue = touchPosX - this.touchedStart;
  }

  @HostListener('touchend', ['$event'])
  onTouchEnd () {
    this.touchActive = false;
    if (this.transformValue > window.innerWidth / 4) {
      this.prev();
    } else if (this.transformValue < -window.innerWidth / 4) {
      this.next();
    } else {
      this.animateState = true;
      setTimeout(() => {
        this.animateState = false;
      }, 600);
    }

    this.transformValue = 0;
  }
}
