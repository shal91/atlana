import { Component } from '@angular/core';

import { ToggleMenuService } from '../../services/toggle-menu.service';
import { NavItem } from '../../interfaces/nav-item';
import {NavigateService} from "../../services/navigate.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent {
  public openMenu: boolean = false;
  public navMenu: NavItem[] = [
    {
      title: 'agency',
      target: 'about',
    },
    {
      title: 'services',
      target: 'services',
    },
    {
      title: 'projects',
      target: 'projects',
    },
    {
      title: 'how we work',
      target: 'how',
    },
    {
      title: 'key points',
      target: 'key-points'
    }
  ];

  constructor (
               private _toggleMenu: ToggleMenuService,
               private _navService: NavigateService) {

    this._toggleMenu.state.subscribe((value) => {
      this.openMenu = value;
    });
  }

  switchMenu () {
    this._toggleMenu.toggle(!this.openMenu);
  }

  closeMenu () {
    this._toggleMenu.toggle(false);
  }

  navClick(fragment) {
    this.closeMenu();
    this._navService.scrollToElem(fragment);
  }

}
