import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Project } from '../../interfaces/project';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { NgxCarousel } from 'ngx-carousel';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.scss']
})
export class ProjectPageComponent implements OnInit {
  public project: Project;
  public id: number;
  private subscription: Subscription;
  public carouselTileOne: NgxCarousel;
  public projects: any[] = [];

  constructor (private _apiService: ApiService,
               private _activateRoute: ActivatedRoute) {

    this.id = this._activateRoute.snapshot.params['id'];
    this._apiService.getProjects().subscribe((value: any) => {
      this.projects = value;
      this.refreshObject();
    });

    this.subscription = this._activateRoute.params.subscribe(params => {
      this.id = params.id;
      this.refreshObject();
    });
  }

  ngOnInit () {
  }

  refreshObject() {
    this.projects.forEach((item: Project) => {
      if (item.id == this.id) {
        this.project = item;
      }
    });
  }

}
