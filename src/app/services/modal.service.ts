import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ModalService {
  public state = new BehaviorSubject(null);

  constructor() { }

  switchState(state) {
    document.body.classList[state === null ? 'remove' : 'add']('body-modal');
    this.state.next(state);
  }
}
