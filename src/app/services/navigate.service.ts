import { HostListener, Injectable, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class NavigateService {

  public routes = {
    prev: '/',
    current: '/'
  };

  scrollTo = new BehaviorSubject(0);
  elem;

  constructor (private _router: Router) {
    // this._router.events.subscribe(s => {
    //     if (s instanceof NavigationEnd) {
    //       const tree = this._router.parseUrl(this._router.url);
    //       if (this.routes.current !== this._router.url) {
    //         this.routes.prev = this.routes.current;
    //         this.routes.current = this._router.url;
    //       }
    //
    //       let mainPageStayed: boolean = false;
    //       if ((this.routes.prev[1] === '#' || this.routes.prev === '/')
    //         && (this.routes.current[1] === '#' || this.routes.current === '/')) {
    //         mainPageStayed = true;
    //       }
    //       if (tree.fragment) {
    //         this.elem = document.querySelector('#' + tree.fragment);
    //             this.changeScrollValue();
    //       } else {
    //           document.documentElement.scrollTop = 0;
    //       }
    //     }
    //   }
    // );
  }

  changeScrollValue () {
    if (this.elem) {
      const el = document.scrollingElement || document.documentElement;
      const dest = el.scrollTop + this.elem.getBoundingClientRect().top;
      this.scrollTo.next(dest);
    }

  }

  scrollToElem(fragment) {
    this.elem = document.querySelector('#' + fragment);
    this.changeScrollValue();
  }
}
