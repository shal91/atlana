import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ToggleMenuService {
  public state = new BehaviorSubject(false);

  constructor() { }

  toggle(state) {
    this.state.next(state);
  }


}
