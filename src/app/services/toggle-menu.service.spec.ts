import { TestBed, inject } from '@angular/core/testing';

import { ToggleMenuService } from './toggle-menu.service';

describe('ToggleMenuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToggleMenuService]
    });
  });

  it('should be created', inject([ToggleMenuService], (service: ToggleMenuService) => {
    expect(service).toBeTruthy();
  }));
});
