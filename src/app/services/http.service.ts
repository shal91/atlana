import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';

@Injectable()
export class HttpService {
  url = 'http://atlana.pro/backend/';

  constructor (private _http: Http) {

  }

  sendMessage (data) {
    return this._http.post(this.url + 'mail/', data).map(
      (response) => {
        console.log(response);
      }, err => { console.log(err)}
    );
  }


}
