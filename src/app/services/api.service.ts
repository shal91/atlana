import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../environments/environment';
import { Project } from '../interfaces/project';


@Injectable()
export class ApiService {

  constructor (private _http: HttpClient) {
  }

  getProjects () {
    return this._http.get('../../assets/data/projects.json');
  }

  getProject (id): Project {
    let project;
    let projects:any = [];
    this.getProjects().subscribe((value) => projects = value);
    projects.forEach((item) => {
      if (item.id === id) {
        project = item;
      }
    });
    return project;
  }

  getServices () {
    return this._http.get('../../assets/data/services.json');
  }

  getWorkflow () {
    return this._http.get('../../assets/data/workflow.json');
  }

  getKeyPoints () {
    return this._http.get('../../assets/data/keypoints.json');
  }

  getCareers() {
    return this._http.get('../../assets/data/careers.json');
  }

  getEngagementModels() {
    return this._http.get('../../assets/data/engagement.json');
  }
}
