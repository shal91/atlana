import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgxCarouselModule } from 'ngx-carousel';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

/*
  Components
 */

import { DialogComponent } from './components/dialog/dialog.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { ProjectPageComponent } from './components/project-page/project-page.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { CareersComponent } from './components/careers/careers.component';

/*
  Services
 */

import { ApiService } from './services/api.service';
import { ModalService } from './services/modal.service';
import { ToggleMenuService} from './services/toggle-menu.service';
import { NavigateService } from './services/navigate.service';
import { HttpService} from './services/http.service';

/*
  Hammerjs implementation
 */

import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { ScreenScaleDirective } from './directives/screen-scale.directive';

import { Http, HttpModule } from '@angular/http';
import { ContactFormComponent } from './components/contact-form/contact-form.component';

export class MyHammerConfig extends HammerGestureConfig {
  overrides = <any>{
    'swipe': {velocity: 0.4, threshold: 20} // override default settings
  };
}

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'project/:id', component: ProjectPageComponent},
  {path: 'careers', component: CareersComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  declarations: [
    AppComponent,
    DialogComponent,
    HeaderComponent,
    HomeComponent,
    ProjectPageComponent,
    CarouselComponent,
    CareersComponent,
    ScreenScaleDirective,
    ContactFormComponent,
  ],
  imports: [
    BrowserModule,
    NgxCarouselModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [
    ApiService,
    ModalService,
    ToggleMenuService,
    NavigateService,
    HttpService,
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
