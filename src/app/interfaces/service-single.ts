export interface ServiceSingle {
  title:string;
  img:string;
  description:string;
  list:string[];
  xsIcon:string;
}
