export interface Letter {
  name: string;
  email: string;
  company: string;
  message?: string;
}
