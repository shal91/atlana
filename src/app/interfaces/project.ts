export interface Project {
  id: number;
  name: string;
  shortDesc ?: string;
  image: string;
  images: string[];
  services: string;
  client?: string;
  date?: string;
  liveLink ?: string;
  about ?: string;
  topBg?: string;
  screens?: string[];
  details?: string[];
}
