export interface NavItem {
  title:string;
  target:string;
}
