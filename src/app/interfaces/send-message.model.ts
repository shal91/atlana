export class SendMessageModel {
  name: string;
  email: string;
  company: string;
  message: string;
}