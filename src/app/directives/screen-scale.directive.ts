import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[screenScale]'
})
export class ScreenScaleDirective {
  scale;
  inited = false;
  offset = 0;
  windowHeight = 0;

  @HostListener('window:resize')
  onResize() {
    this.windowHeight = window.innerHeight;
  }

  @HostListener('window:scroll', ['$event'])

  onScroll(e) {
    if (!this.inited) {
      this.offset = this._elementRef.nativeElement.offsetTop;
    }

    let rate = 0;
    if (this.offset > document.documentElement.scrollTop &&
      this.offset < (document.documentElement.scrollTop + this.windowHeight)) {
      
    } else {

    }

  }

  constructor(private _elementRef: ElementRef) {
    this.onResize();
  }

}
