<?php

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) && $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'POST') {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: X-Requested-With, content-type, access-control-allow-origin, access-control-allow-methods, access-control-allow-headers');
    }
    exit;
}

$json = file_get_contents('php://input');
$post = (array)json_decode($json);

if(!empty($post['name']) && !empty($post['email']) && !empty($post['message'])) {
    $time = time();
    session_start();
    if ($_SESSION['user'] && isset($_SESSION['date']) && (($_SESSION['send_date'] - $time) > 60 )) {
        header('Too Many Requests', true, 429);
    } else {
        session_start();
        $_SESSION['user'] = true;
        $_SESSION['send_date'] = $time;

        $to = "hi@atlana.pro"; // this is your Email address
        $first_name = filter_var( $post['name'], FILTER_SANITIZE_STRING);
        $from       = filter_var( $post['email'], FILTER_SANITIZE_EMAIL); // this is the sender's Email address
        $company    = !empty($post['company']) ? filter_var( $post['company'], FILTER_SANITIZE_STRING) : '';
        $subject    = "New message from site";
        $message    = sprintf(file_get_contents('./notification_email.html'), $first_name, $from, $company, nl2br(filter_var($post['message']), FILTER_SANITIZE_STRING));

        $headers = "From: hi@atlana.pro" . PHP_EOL;
        $headers .= "MIME-Version: 1.0" . PHP_EOL;
        $headers .= "Content-Type: text/html; charset=UTF-8" . PHP_EOL;
        $headers .= "X-Priority: 1 (Highest)" . PHP_EOL;
        $headers .= "X-MSMail-Priority: High" . PHP_EOL;
        $headers .= "Importance: High" . PHP_EOL;

        $subject_for_sender = "Ha-ha, now we have your mail! :-)";
        $message_for_sender = str_replace('*NAME*', $first_name, file_get_contents('./followup_email.html'));

        $headers_for_sender = "From:" . $to . PHP_EOL;
        $headers_for_sender .= "MIME-Version: 1.0" . PHP_EOL;
        $headers_for_sender .= "Content-Type: text/html; charset=UTF-8" . PHP_EOL;
        $headers_for_sender .= "X-Priority: 1 (Highest)" . PHP_EOL;
        $headers_for_sender .= "X-MSMail-Priority: High" . PHP_EOL;
        $headers_for_sender .= "Importance: High" . PHP_EOL;

        mail($to, $subject, $message, $headers); // notification message for us
        mail($from, $subject_for_sender, $message_for_sender, $headers_for_sender); // sends a message to sender

        header('HTTP/1.1 200 OK', true, 200);
    }
} else {
    header('HTTP/1.1 400 Bad Request', true, 400);
}


header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');

?>
